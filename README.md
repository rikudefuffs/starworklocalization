# Introduction

This package contains a localization framework that helps you localize your game in several languages.

# Setup

Add the following line to the dependencies, in the manifest.json of the project, to install the package:
"com.starworkgc.localization": "https://gitlab.com/rikudefuffs/starworklocalization.git"

Then open your unity project and go to "Starwork > Localization > Generate localization file". This will generate a Localization file in a "Resources" folder of your project (see Unity's console for more details after you run the command), which will then be used to localize your application.

Add the following line to the manifest.json of the project (or unit tests won't be displayed in the test runner)
"testables": ["com.starworkgc.localization"],

# Usage

If you're trying the tests: just run them from the Test Runner.
If you're adding the package for development, the Localization.cs file contains all the info. 
Spoiler: You just need to call Localization.Get(<localization key>) once the localization file is loaded.
Just be sure to define your localization CSV file and to load it from wherever you want.
If you want to localize a label in the UI, just add a LocalizeText component to it.

# Feedback

Feedback is much appreciated, just reach out to the authors/mantainers by email or linkedin

Enjoy!

Paolo