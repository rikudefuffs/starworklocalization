KEY,English,Italiano,Français,Español
Language,English,Italiano,Français,Español
English,English
Italiano,Italiano
Français,Français
Español,Español

MainMenu,"Main menu","Menu principale","Menu principal","Menu principal"
StartGame,"Start Game","Inizia Partita","Lancer le match","Empieza el partido"
Options,"Options","Opzioni","Opcioness","Ajustes"
ShowLeaderboard,"Global leaderboard","Classifica globale","Classement général","Clasificación mundial"
QuitGame,"Quit","Esci","Quitter le jeu","Salir del juego"

DisplayResolution,"Display Resolution","Risoluzione Schermo","Résolution d'affichage","Resolución de pantalla"
Volume,"Volume","Volume","Volume","Volumen"
WindowMode,"Window Mode","Modalità finestra","Mode d'affichage","Modo ventana"
WindowMode_Fullscreen,"Fullscreen","Schermo intero","Plain écran","Pantalla completa"
WindowMode_Window,"Window","Finestra","Plain écran fenêtre","Pantalla completa en ventana"
Quality,"Quality","Qualità","Qualité","Calidad"
Quality_Low,"Low","Basso","Faible","Baja"
Quality_Medium,"Medium","Medio","Moyen","Media"
Quality_High,"High","Alto","Elevé","Alto"
Quality_Epic,"Epic","Epico","Epique","Epica"
Back,"Back","Indietro","Retour","Espalda"
Apply,"Apply","Applica","Appliquer","Aplicar"
SelectLanguage,"Language","Lingua","Langue","Idioma"
Leaderboard,"Leaderboard","Classifica","Classement","Clasificación"