# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.1] - 2020-04-06
### Added
- Added CTRL + ALT + L shortcut to reload the localization file without going into/out play mode

## [1.0.0] - 2020-04-06
### Added
- Added core functionalities of the framework